package fr.emiage.sorttab;


import fr.emiage.sorttab.SortTab;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 *
 * @author cyril
 */
public class SortTabTest {
    
    private int[] tab;
    private int[] a1;
    private int[] a2;
    private int[] b0;
    private int[] b1;
    private int[] b2;
    private int[] c1;
    private int[] c2;
    
    public SortTabTest() {
    }
    
   static void tearDownClass() {
    }
    
    @Before  
    public void setUp() {
    a1 = new int [] {1};
    a2 = new int [] {1};
    b0 = new int [] {0,1,2,3,4,5,6,7,8,9};
    b1 = new int []  {0,1,2,3,4,5,6,7,8,9};
    b2 = new int []  {9,8,7,6,5,4,3,2,1,0};
    c1 = new int []  {5,1,2,4,4,3};
    c2 = new int []  {1,2,3,4,4,5};     
    
     tab =  SortTab.randomTab(25);
        
    }
    
    @After
    public void tearDown() {
    }

    
 boolean isSorted(int[] array) {
        if (array.length==1)
            return true;
        else
        {
        for (int i = 1; i < array.length; ++i)
            if (array[i] < array[i - 1])
                return false;
                return true;
        }
}
    /**
     * Test of randomTab method, of class SortTab.
     */
    @org.junit.Test
    public void testRandomTab() {
        System.out.println("randomTab");       
          
        int size2 = 10;      
        int[] result2 = SortTab.randomTab(size2);
        assertEquals(size2, result2.length);
        
     System.out.println("randomTab : ok ");
    }



    /**
     * Test of selectSort method, of class SortTab.
     */
    @org.junit.Test
    public void testSelectSort() {
        System.out.println("selectSort");
        
        
         SortTab.selectSort(tab);
         
         
        assertTrue(isSorted(tab));
        
        SortTab.selectSort(a2);
         SortTab.selectSort(b1);
         SortTab.selectSort(b2);
         SortTab.selectSort(c1);
        assertArrayEquals(a1, a2);
        assertTrue(isSorted(c1));
      assertArrayEquals(c1, c2);
       
    }

    /**
     * Test of insertSort method, of class SortTab.
     */
    @org.junit.Test
    public void testInsertSort() {
        System.out.println("insertSort");
         SortTab.insertSort(tab);
        assertTrue(isSorted(tab));
         SortTab.insertSort(a1);
        SortTab.insertSort(c1);
         assertArrayEquals(a1, a2);
        assertTrue(isSorted(c1));
      assertArrayEquals(c1, c2);
       
       
    }

    /**
     * Test of quickSort method, of class SortTab.
     */
    @org.junit.Test
    public void testQuickSort() {
        System.out.println("quickSort");
         SortTab.quickSort(tab);
        assertTrue(isSorted(tab));
       //  SortTab.quickSort(a1);
       
        
    }

    /**
     * Test of mergeSort method, of class SortTab.
     */
    @org.junit.Test
    public void testMergeSort() {
        System.out.println("mergeSort");
        SortTab.mergeSort(tab);
        assertTrue(isSorted(tab));
        SortTab.mergeSort(a1);
       assertTrue(isSorted(a1));
       
    }

    /**
     * Test of main method, of class SortTab.
     */
    @org.junit.Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        SortTab.main(args);
        
    }
    
}