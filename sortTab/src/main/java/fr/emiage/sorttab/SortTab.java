/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.emiage.sorttab;

/**
 * Algorithmes de tri
 * @author cyril
 */


import java.util.Random;


public final class SortTab {

    /**
     * Constructeur privee pour empecher l'instantiation de classe. *
     */
    private SortTab() {
    }

    /**
     * Generation d'un tableau d'entiers aleatoires.
     *
     * @param size taille du tableau
     * @return un tableau d'entiers aleatoires
     */
    public static int[] randomTab(final int size) {
        Random rand = new Random();
        int[] tab = new int[size];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = rand.nextInt();
        }
        return tab;
    }

    /**
     * Fonction d'échange d'entiers.
     *
     */
    private static void echanger(int[] tableauTrie, int i, int j) {

        int temp = tableauTrie[i];
        tableauTrie[i] = tableauTrie[j];
        tableauTrie[j] = temp;

    }

    /**
  
    /**
     * Tri par selection.
     *
     * @param tab
      *
     */
    public static void selectSort(final int[] tab) {
   
      
              int n = tab.length;

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < n - 1; i++) {
            // Find the minimum element in unsorted array
            int min_idx = i;
            for (int j = i + 1; j < n; j++) {
                if (tab[j] < tab[min_idx]) {
                    min_idx = j;
                }
            }

            // Swap the found minimum element with the first
            // element
            int temp = tab[min_idx];
            tab[min_idx] = tab[i];
            tab[i] = temp;
        
       }
    }

    /**
     * Tri par insertion
     *
     * @param tab
      *
     */
    public static void insertSort(final int[] tab) {

        int i, j, cle;

        for (i = 1; i < tab.length; i++) {
            cle = tab[i];
            j = i;
            while ((j >= 1) && (tab[j - 1] > cle)) {
                tab[j] = tab[j - 1];
                j = j - 1;
            }
            tab[j] = cle;
        }
    }

    /**
     * Tri rapide.
      *
     * @param tab
     */
    public static void quickSort(final int[] tab) {

        int begin = 0;
        int end = tab.length - 1;        
        quickSort2(tab, begin, end);
    }

    private static void quickSort2(int arr[], int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);
            quickSort2(arr, begin, partitionIndex - 1);
            quickSort2(arr, partitionIndex + 1, end);
        }
    }

    private static int partition(int arr[], int begin, int end) {
        int pivot = arr[end];
        int i = (begin - 1);

        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;

                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        int swapTemp = arr[i + 1];
        arr[i + 1] = arr[end];
        arr[end] = swapTemp;

        return i + 1;
    }

    /**
     * Tri fusion.
      *
     * @param tab
     */
    public static void mergeSort(final int[] tab) {   
        if (tab.length > 1)
        mergeSort2(tab,tab.length);
    }

    private static void mergeSort2(int[] a, int n) {
         if (n < 2) {
        return;
        }           
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        System.arraycopy(a, 0, l, 0, mid);
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort2( l , mid);
        mergeSort2(r, n - mid);
        merge(a, l, r, mid, n - mid);        
         
      
    }

    private static void merge(
            int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            } else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }

    /**
     * Programme principal.
     *
     * @param args Arguments du programme: taille du tableau
     */
  public static void main(String[] args) {
       
        
        
        int[] SizeTab= {10,100,1000};
        
        for (int i = 0; i < SizeTab.length; i++) {
        
            
        int[] tab = randomTab(SizeTab[i]);
        
        int[] tab_quickSort = tab;
        int[] tab_mergeSort = tab;
        int[] tab_selectSort = tab;
        int[] tab_insertSort = tab;        
    
        System.out.println("--------------\n");        
         System.out.println("---- Generation with size "+SizeTab[i]+"\n");
     
        
            

        
         System.out.println("----quick Sort ----\n");
         long debut2 = System.currentTimeMillis();
           quickSort(tab_quickSort);
         System.out.println(System.currentTimeMillis()-debut2);
 
        
         System.out.println("----merge Sort ----\n");
           long debut3 = System.currentTimeMillis();
        mergeSort(tab_mergeSort);
           System.out.println(System.currentTimeMillis()-debut3);
    //    System.out.println(Arrays.toString(tab_mergeSort));
        
          System.out.println("----select Sort ----\n");
          long debut4 = System.currentTimeMillis();
        selectSort(tab_selectSort);
           System.out.println(System.currentTimeMillis()-debut4);
   //    System.out.println(Arrays.toString(tab_selectSort));
        
         System.out.println("----insert Sort ----\n");
          long debut5 = System.currentTimeMillis();
        insertSort(tab_insertSort);
         System.out.println(System.currentTimeMillis()-debut5);
    //    System.out.println(Arrays.toString(tab_insertSort));
         System.out.println("--------------\n");
        }
    }
}
