/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sorttab;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 *
 * @author cyril
 */
public class SortTabTest {
    
    private int[] tab;
    private int[] a1;
    private int[] a2;
    
    public SortTabTest() {
    }
    
   static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
     a1 = new int[1];
     a2 = new int[1];  
     tab =  SortTab.randomTab(25);
        
    }
    
    @After
    public void tearDown() {
    }

    
 boolean isSorted(int[] array) {
        for (int i = 1; i < array.length; ++i)
            if (array[i] < array[i - 1])
                return false;
                return true;
}
    /**
     * Test of randomTab method, of class SortTab.
     */
    @org.junit.Test
    public void testRandomTab() {
        System.out.println("randomTab");
          
     
        
        int size2 = 10;      
        int[] result2 = SortTab.randomTab(size2);
        assertEquals(size2, result2.length);
        
     
    }

    /**
     * Test of bubbleSort method, of class SortTab.
     */
    @org.junit.Test
    public void testBubbleSort() {
        System.out.println("bubbleSort");
      
        SortTab.bubbleSort(tab);
        assertTrue(isSorted(tab));
         SortTab.bubbleSort(a2);
        assertArrayEquals(a1, a2);
       
      
    }

    /**
     * Test of selectSort method, of class SortTab.
     */
    @org.junit.Test
    public void testSelectSort() {
        System.out.println("selectSort");
         SortTab.selectSort(tab);
        assertTrue(isSorted(tab));
         SortTab.selectSort(a2);
        assertArrayEquals(a1, a2);
       
    }

    /**
     * Test of insertSort method, of class SortTab.
     */
    @org.junit.Test
    public void testInsertSort() {
        System.out.println("insertSort");
         SortTab.insertSort(tab);
        assertTrue(isSorted(tab));
         SortTab.insertSort(a2);
        assertArrayEquals(a1, a2);
       
    }

    /**
     * Test of quickSort method, of class SortTab.
     */
    @org.junit.Test
    public void testQuickSort() {
        System.out.println("quickSort");
         SortTab.quickSort(tab);
        assertTrue(isSorted(tab));
         SortTab.quickSort(a2);
        assertArrayEquals(a1, a2);
        
    }

    /**
     * Test of mergeSort method, of class SortTab.
     */
    @org.junit.Test
    public void testMergeSort() {
        System.out.println("mergeSort");
           SortTab.mergeSort(tab);
        assertTrue(isSorted(tab));
         SortTab.mergeSort(a2);
        assertArrayEquals(a1, a2);
       
    }

    /**
     * Test of main method, of class SortTab.
     */
    @org.junit.Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        SortTab.main(args);
        
    }
    
}
